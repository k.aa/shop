﻿using Microsoft.AspNetCore.Mvc;
using shop.Data.Interfaces;
using shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shop.Controllers
{
    public class HomeController:Controller
    {
        private readonly iAllCars _carRep;
        
        public HomeController(iAllCars carRep)
        {
            _carRep = carRep;
        }

        public ViewResult Index()
        {
            var homeCars = new HomeViewModel
            {
                favCars = _carRep.getFavCars
            };
            return View(homeCars);
        }
    }
}

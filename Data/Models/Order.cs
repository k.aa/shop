﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace shop.Data.Models
{
    public class Order
    {
        [BindNever]
        public int id { get; set; }

        [Display(Name = "Имя")]
        [StringLength(10)]
        [Required(ErrorMessage = "Длина имени не менее 10 символов")]
        public string name { get; set; }

        [Display(Name = "Фамилия")]
        [StringLength(5)]
        [Required(ErrorMessage = "Длина фамилии не менее 5 символов")]
        public string surname { get; set; }

        [Display(Name = "Адрес")]
        [StringLength(10)]
        [Required(ErrorMessage = "Длина адреса не менее 10 символов")]
        public string address { get; set; }

        [Display(Name = "Телефон")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(11)]
        [Required(ErrorMessage = "Длина телефона не менее 11 символов")]
        public string phone { get; set; }

        [Display(Name = "Почта")]
        [DataType(DataType.EmailAddress)]
        [StringLength(12)]
        [Required(ErrorMessage = "Длина почты не менее 12 символов")]
        public string email { get; set; }

        [BindNever]
        [ScaffoldColumn(false)]
        public DateTime orderTime { get; set; }

        public List<OrderDetail> orderDetails { get; set; }

    }
}

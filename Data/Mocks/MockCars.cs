﻿using shop.Data.Interfaces;
using shop.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shop.Data.Mocks
{
    public class MockCars : iAllCars
    {
        private readonly iCarsCategory _categoryCars = new MockCategory();
        public IEnumerable<Car> Cars {
            get
            {
                return new List<Car> {
                new Car
                {
                    name = "Tesla",
                    shortDesc = "",
                    longDesc = "",
                    img = "/img/1.jpg",
                    price = 45000,
                    isFavorite = true,
                    avaible = true,
                    Category = _categoryCars.AllCategories.First()
                },
                new Car
                {
                    name = "Nissan",
                    shortDesc = "",
                    longDesc = "",
                    img = "/img/2.jpg",
                    price = 45000,
                    isFavorite = true,
                    avaible = true,
                    Category = _categoryCars.AllCategories.First()
                },
                new Car
                {
                    name = "Towota",
                    shortDesc = "",
                    longDesc = "",
                    img = "/img/3.jpg",
                    price = 45000,
                    isFavorite = true,
                    avaible = true,
                    Category = _categoryCars.AllCategories.Last()
                },
                new Car
                {
                    name = "VW",
                    shortDesc = "",
                    longDesc = "",
                    img = "/img/4.jpg",
                    price = 45000,
                    isFavorite = true,
                    avaible = true,
                    Category = _categoryCars.AllCategories.Last()
                }
            };
            }
        }
        public IEnumerable<Car> getFavCars { get; set; }

        public Car getObjectCar(int carID)
        {
            throw new NotImplementedException();
        }
    }
}

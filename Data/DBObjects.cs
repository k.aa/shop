﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using shop.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shop.Data
{
    public class DBObjects
    {
        private static Dictionary<string, Category> category;
        public static void Initial(AppDBContent content)
        {
            if (!content.Category.Any())
            {
                content.Category.AddRange(Categories.Select(c => c.Value));
            }

            if (!content.Car.Any())
            {
                content.AddRange(
                    new Car
                    {
                        name = "Tesla",
                        shortDesc = "",
                        longDesc = "",
                        img = "/img/1.jpg",
                        price = 45000,
                        isFavorite = true,
                        avaible = true,
                        Category = Categories["Электромобили"]
                    },
                    new Car
                    {
                        name = "Nissan",
                        shortDesc = "",
                        longDesc = "",
                        img = "/img/2.jpg",
                        price = 45000,
                        isFavorite = false,
                        avaible = true,
                        Category = Categories["Электромобили"]
                    },
                    new Car
                    {
                        name = "Towota",
                        shortDesc = "",
                        longDesc = "",
                        img = "/img/3.jpg",
                        price = 45000,
                        isFavorite = true,
                        avaible = true,
                        Category = Categories["Классические автомобили"]
                    },
                    new Car
                    {
                        name = "VW",
                        shortDesc = "",
                        longDesc = "",
                        img = "/img/4.jpg",
                        price = 45000,
                        isFavorite = false,
                        avaible = true,
                        Category = Categories["Классические автомобили"]
                    }
                );
            }

            content.SaveChanges();
        }

        public static Dictionary<string, Category> Categories
        {
            get
            {
                if(category == null)
                {
                    var  list = new Category[]
                    {                        
                        new Category { categoryName = "Электромобили", desc = "Современный вид транспорта" },
                        new Category { categoryName = "Классические автомобили", desc = "Машины с двигателем внутреннего сгорания" }
                    };

                    category = new Dictionary<string, Category>();
                    foreach (Category el in list)
                    {
                        category.Add(el.categoryName, el);
                    }
                }

                return category;
            }
        }
    }
}
